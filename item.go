package torrentapi

// Item represents a torrent item.
type Item struct {
	Title    string  `json:"title"`
	Category string  `json:"category"`
	Hash     string  `json:"download"`
	Seeders  int     `json:"seeders"`
	Leechers int     `json:"leechers"`
	PubDate  PubDate `json:"pubdate"`
	Size     int64   `json:"size"`
	Meta     Meta    `json:"episode_info"`
}

// Meta represents a torrent item meta data.
type Meta struct {
	IMDB       string `json:"imdb"`
	TheMovieDB string `json:"themoviedb"`
	TVDB       string `json:"tvdb"`
}

type itemHolder struct {
	Items []Item `json:"torrent_results"`
}
