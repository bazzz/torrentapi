package torrentapi

import (
	"fmt"
	"testing"
)

func init() {
	AppID = "testing"
}

func TestListWithoutCategories(t *testing.T) {
	items, err := List([]Category{})
	if err != nil {
		t.Fatal(err)
	}
	if len(items) < 1 {
		t.Log("items was empty")
		t.Fail()
	}
}

func TestListWithCategories(t *testing.T) {
	categories := []Category{
		MoviesX264,
		MoviesX264_1080,
		MoviesX264_4k,
		MoviesX265_1080,
		MoviesX265_4k,
	}
	items, err := List(categories)
	if err != nil {
		t.Fatal(err)
	}
	if len(items) < 1 {
		t.Log("items was empty")
		t.Fail()
	}
	for _, item := range items {
		t.Log(item)
	}
}

func TestEpisodesByTVDB(t *testing.T) {
	tvdb := 328634
	items, err := Episodes(tvdb)
	if err != nil {
		t.Fatal(err)
	}
	if len(items) < 1 {
		t.Log("items was empty")
		t.Fail()
	}
}

func TestEpisodesByIMDB(t *testing.T) {
	id := "tt3230854"
	items, err := EpisodesByIMDB(id)
	if err != nil {
		t.Fatal(err)
	}
	if len(items) < 1 {
		t.Log("items was empty")
		t.Fail()
	}
	for _, item := range items {
		fmt.Println(item)
	}
}

func TestSearchTV(t *testing.T) {
	text := "The Expanse"
	items, err := SearchTV(text)
	if err != nil {
		t.Fatal(err)
	}
	for _, item := range items {
		fmt.Println(item)
	}
}
