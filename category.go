package torrentapi

// Category represents a torrent category from the rarbg.to website.
type Category int

const (
	XXX              Category = 4
	MoviesXVID       Category = 14
	MoviesXVID_720   Category = 48
	MoviesX264       Category = 17
	MoviesX264_1080  Category = 44
	MoviesX264_720   Category = 45
	MoviesX264_3D    Category = 47
	MoviesX264_4k    Category = 50
	MoviesX265_4k    Category = 51
	MoviesX265_4kHDR Category = 52
	MoviesFullBD     Category = 42
	MoviesBDremux    Category = 46
	MoviesX265_1080  Category = 54
	TVEpisodes       Category = 18
	TVHDEpisodes     Category = 41
	TVUHDEpisodes    Category = 49
	MusicMP3         Category = 23
	MusicFLAC        Category = 25
	GamesPC_ISO      Category = 27
	GamesPC_RIP      Category = 28
	GamesPS3         Category = 40
	GamesXBOX360     Category = 32
	SoftwarePC_ISO   Category = 33
	GamesPS4         Category = 53
)
