package torrentapi

import (
	"time"
)

// PubDate holds a datetime.
type PubDate struct {
	time.Time
}

// UnmarshalJSON overrides the default JSON Unmarshaller.
func (t *PubDate) UnmarshalJSON(b []byte) error {
	s := string(b)
	result, _ := time.Parse("2006-01-02 15:04:05 -0700", s[1:len(s)-1])
	*t = PubDate{result}
	return nil
}
