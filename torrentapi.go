package torrentapi

import (
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	baseURL      = "https://torrentapi.org/pubapi_v2.php?format=json_extended&app_id="
	magnetPrefix = "magnet:?xt=urn:btih:"
)

// AppID is a name for the application that makes the call, as torrentapi asks for it.
var AppID = ""

var (
	token *tokenHolder
)

// List returns the 100 most recent torrents for the specified categories. If categories is empty, it will list everything.
func List(categories []Category) ([]Item, error) {
	url := baseURL + AppID + "&mode=list&limit=100"
	if len(categories) > 0 {
		url += "&category=" + flattenCategories(categories)
	}
	return callTorrentAPI(url)
}

// Search returns found torrents for the given text.
func Search(text string) ([]Item, error) {
	url := baseURL + AppID + "&mode=search&search_string=" + url.QueryEscape(text)
	return callTorrentAPI(url)
}

// SearchTV returns found torrents for the given text in the 'tv' category.
func SearchTV(text string) ([]Item, error) {
	url := baseURL + AppID + "&mode=search&category=tv&search_string=" + url.QueryEscape(text)
	return callTorrentAPI(url)
}

// DEPRECATED use EpisodesByIMDB instead.
// Searching by TVDB and/or TheMovieDB sometimes gives no results, even though the id is correct.
func Episodes(TVDBShowID int) ([]Item, error) {
	url := baseURL + AppID + "&category=tv&mode=search&search_tvdb=" + strconv.Itoa(TVDBShowID)
	return callTorrentAPI(url)
}

// EpisodesByIMDB returns the most recent torrents for the TV show specified by the IMDB id parameter.
func EpisodesByIMDB(id string) ([]Item, error) {
	if !strings.HasPrefix(id, "tt") {
		return nil, errors.New("id is not a valid imdb id")
	}
	url := baseURL + AppID + "&category=tv&mode=search&limit=100&search_imdb=" + id
	return callTorrentAPI(url)
}

func callTorrentAPI(url string) ([]Item, error) {
	if AppID == "" {
		return nil, errors.New("torrentapi.AppID cannot be empty, set the AppID first")
	}
	now := time.Now()
	if token == nil || now.Sub(token.Timestamp) < (15*time.Minute) {
		err := refreshToken()
		if err != nil {
			return nil, err
		}
	}
	url += "&token=" + token.Token

	attempts := 1
	for attempts <= 10 {
		time.Sleep(time.Duration(attempts*2) * time.Second) // Wait for increasingly more seconds to avoid calling too quickly and get HTTP status code 429, starting at 2 secs because the docs say 'The api has a 1req/2s limit'.
		data, err := call(url)
		if err != nil {
			return nil, err
		}
		if strings.HasPrefix(string(data), "{\"error\":\"No results found\",\"error_code\":20") { // When searching by TVDB or TheMovieDB id, sometimes this is returned even though the id is correct.
			return nil, nil
		}
		holder := itemHolder{}
		err = json.Unmarshal(data, &holder)
		if err != nil {
			return nil, err
		}
		items := holder.Items
		for i := 0; i < len(items); i++ { // Remove tracker information and store only the hash.
			items[i].Hash = items[i].Hash[len(magnetPrefix) : len(magnetPrefix)+40]
		}
		return items, nil
	}
	return nil, errors.New("made 10 attempts, did not get any results")
}

func refreshToken() error {
	if token == nil {
		token = &tokenHolder{}
	}
	data, err := call(baseURL + AppID + "&get_token=get_token")
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, token)
	if err != nil {
		return err
	}
	return nil
}

func call(url string) (data []byte, err error) {
	response, err := http.Get(url)
	if err != nil {
		return
	}
	if response.StatusCode != 200 {
		err = errors.New("torrentAPI: " + response.Status)
		return
	}
	defer response.Body.Close()
	data, err = io.ReadAll(response.Body)
	return
}

func flattenCategories(categories []Category) string {
	s := make([]string, 0)
	for _, c := range categories {
		s = append(s, strconv.Itoa(int(c)))
	}
	return strings.Join(s, ";")
}
