package torrentapi

import "time"

type tokenHolder struct {
	Token     string `json:"token"`
	Timestamp time.Time
}
